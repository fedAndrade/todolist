# Herramientas usadas

- Spring Boot 2.4.4

- MySQL 8.0

- Java SE 14

- Maven 3.6.3

- Axios 0.21

- JQuery 3.6

- Bootstrap 4.1.3

- Yarn
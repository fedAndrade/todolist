package com.todolist.webapp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/todolist")
public class AppController  
{
    @Autowired
    TaskRepository taskDB;    

    @GetMapping("/tasks")
    public List<Task> getTasks(Model model)
    {
        return taskDB.findAll();
    }

    @PostMapping("/tasks/{name}")
    public List<Task> createTask(@PathVariable("name")  String name) {
        taskDB.save(new Task(name));
        return taskDB.findAll();
    }
  

    @GetMapping("/tasks/{id}")
    public Task getTaskById(@PathVariable("id") Long id) {
        return taskDB.findById(id).get();
    }

    @PutMapping("/tasks/{id}")
    public void checkTask(@PathVariable("id") Long id){
        Task temp = taskDB.getOne(id);
        temp.setCompleted(!temp.isCompleted());
        taskDB.save(temp);
    }    

    @PutMapping("/tasks/{id}/{name}")
    public List<Task> Task(@PathVariable("id") Long id, @PathVariable("name") String name){
        Task temp = taskDB.getOne(id);
        temp.setName(name);
        taskDB.save(temp);
        return taskDB.findAll();
    }

    @DeleteMapping("/tasks/{id}")
    public List<Task> deleteTask(@PathVariable("id") Long id) {
        taskDB.deleteById(id);
        return taskDB.findAll();
    }
}

package com.todolist.webapp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Task 
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    private boolean completed;

    public Task()
    {
        
    }

    public Task(String name)
    {
        this.name = name;
        completed = false;
    }

    public String getName() 
    {
        return name;
    }

    public void setName(String name) 
    {
        this.name = name;
    }

    public Boolean isCompleted() 
    {
        return completed;
    }

    public void switchCompleted() 
    {
        completed = !completed;
    }

    public void setCompleted(boolean completed) 
    {
        this.completed = completed;
    }

    public Long getId() 
    {
        return id;
    }

    public void setId(Long id) 
    {
        this.id = id;
    }
}

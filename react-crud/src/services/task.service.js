import axios from 'axios'

const TASK_REST_API_URL = 'http://localhost:8080/todolist/tasks';

class TaskService {

    getTasks(){
        return axios.get(TASK_REST_API_URL);
    }

    deleteTasks(id){
        return axios.delete(`${TASK_REST_API_URL}/${id}`);
    }

    checkTasks(id){
        return axios.put(`${TASK_REST_API_URL}/${id}`);
    }

    addTasks(name){
        return axios.post(`${TASK_REST_API_URL}/${name}`);
    }
    
    editTasks(id, name){
        return axios.put(`${TASK_REST_API_URL}/${id}/${name}`)
    }
}

export default new TaskService();


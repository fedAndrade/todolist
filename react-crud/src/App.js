import './App.css';
import React, {Component} from 'react';
import TaskService from './services/task.service';
import * as $ from 'jquery'; 

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {tasks: [], createName: '', edited: '', editNewName: '', editing: false, boolean: {}};
    this.handleChange = this.handleChange.bind(this);
    this.handleEditChange = this.handleEditChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleEdit = this.handleEdit.bind(this);
    this.checkboxHandleChange = this.checkboxHandleChange.bind(this);
  }

  handleChange(event) {    
    this.setState({createName: event.target.value});  
  }

  handleEditChange(event) {
    this.setState({editNewName: event.target.value})
  }

  handleEdit(event) {
    this.setState({ editing: false });
    this.editTasks(this.state.edited, this.state.editNewName);
    event.preventDefault();
  }

  checkboxHandleChange(id) {
    var map = this.state.boolean;
    map[id] = !map[id];
    this.setState( {boolean: map} )
  }

  handleSubmit(event) {
    this.submitTasks(this.state.createName);
    this.setState({createName: ''});
    event.preventDefault();
  }

  componentDidMount() {
    this.refreshTasks();
  }

  refreshTasks() {
    TaskService.getTasks()
    .then((response) => {
      this.setState({ tasks: response.data });    
      var checkedMap = {};
      response.data.map((task) => (
        checkedMap[task.id] = !!task.completed
      ))
      this.setState({ boolean: checkedMap });

    })
    .catch(console.log);
  }

  checkTasks(id) {
    TaskService.checkTasks(id)
  }

  submitTasks(name) {
    TaskService.addTasks(name)
    .then((response) => {
      this.setState({ tasks: response.data })
    })
    .catch(console.log);
  }

  editTasks(id, name){
    $('#text'+id).html(name);
    TaskService.editTasks(id, name)
    .then((response) => {
      this.setState({ tasks: response.data })
    })
    .catch(console.log);
  }

  deleteTasks(id){
    TaskService.deleteTasks(id)
    .then((response) => {
      this.setState({ tasks: response.data })
    })
    .catch(console.log);
  }

  beginEdit(id){
    this.setState({ edited: id, editing: true });
  }

  render () {
    return (
      <div class="container col-md-4 offset-md-4">
        <h3>To Do List</h3>
        <table class="form-group mb-2">
          <tbody>
            {this.state.tasks.map((task) => (
              <tr id={'task'+task.id}>
                <td class="form-check"><input class="form-check-info" type="checkbox" checked={this.state.boolean[task.id]} onClick={() => this.checkTasks(task.id)} onChange={() => this.checkboxHandleChange(task.id)}/></td>
                <td><p id={'text'+task.id}>{task.name}</p></td>
                <td><button class="btn btn-warning" onClick={() => this.beginEdit(task.id)}>Edit</button></td>
                <td><button class="btn btn-danger" onClick={() => this.deleteTasks(task.id)}>Delete</button></td>
              </tr>
            ))}
          </tbody>
        </table>
        {this.state.editing ?
        (<form class="form-inline" onSubmit={this.handleEdit}>
          <div class="form-group  mx-sm-3 mb-2">
            <input class="form-control" type="text" placeholder="Edit Task" value={this.state.editNewName} onChange={this.handleEditChange}/>
          </div>
          <input class="btn btn-info  mb-2" type="submit" value="Edit"/>
        </form>
        ) : null}
        <form class="form-inline" onSubmit={this.handleSubmit}>
          <div class="form-group  mx-sm-3 mb-2">
            <input class="form-control" type="text" placeholder="New Task" value={this.state.createName} onChange={this.handleChange}/>
          </div>
          <input class="btn btn-info  mb-2" type="submit" value="Add"/>
        </form>
      </div>
    );
  }
}

export default App;
